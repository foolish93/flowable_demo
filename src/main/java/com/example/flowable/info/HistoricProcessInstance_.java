package com.example.flowable.info;

import org.flowable.engine.history.HistoricProcessInstance;
import org.flowable.engine.runtime.ProcessInstance;

import java.util.Date;

/**
 * @author Foolish
 * @description
 * @date: 2018/9/26 13:40
 */
public class HistoricProcessInstance_ {
    String id;
    String deploymentId;
    String description;
    String name;
    String tenantId;
    String businessKey;
    String callbackId;
    String callbackType;
    String processDefinitionId;
    String processDefinitionKey;
    String processDefinitionName;
    String processDefinitionVersion;
    Date startTime;
    String startUserId;

    public HistoricProcessInstance_(HistoricProcessInstance pi) {
        this.id = pi.getId();
        this.deploymentId = pi.getDeploymentId();
        this.description = pi.getDescription();
        this.name = pi.getName();
        this.tenantId = pi.getTenantId();
        this.businessKey = pi.getBusinessKey();
        this.callbackId = pi.getCallbackId();
        this.callbackType = pi.getCallbackType();
        this.processDefinitionId = pi.getProcessDefinitionId();
        this.processDefinitionKey = pi.getProcessDefinitionKey();
        this.processDefinitionName = pi.getProcessDefinitionName();
        this.processDefinitionVersion = pi.getProcessDefinitionId();
        this.startTime = pi.getStartTime();
        this.startUserId = pi.getStartUserId();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDeploymentId() {
        return deploymentId;
    }

    public void setDeploymentId(String deploymentId) {
        this.deploymentId = deploymentId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getBusinessKey() {
        return businessKey;
    }

    public void setBusinessKey(String businessKey) {
        this.businessKey = businessKey;
    }

    public String getCallbackId() {
        return callbackId;
    }

    public void setCallbackId(String callbackId) {
        this.callbackId = callbackId;
    }

    public String getCallbackType() {
        return callbackType;
    }

    public void setCallbackType(String callbackType) {
        this.callbackType = callbackType;
    }

    public String getProcessDefinitionId() {
        return processDefinitionId;
    }

    public void setProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
    }

    public String getProcessDefinitionKey() {
        return processDefinitionKey;
    }

    public void setProcessDefinitionKey(String processDefinitionKey) {
        this.processDefinitionKey = processDefinitionKey;
    }

    public String getProcessDefinitionName() {
        return processDefinitionName;
    }

    public void setProcessDefinitionName(String processDefinitionName) {
        this.processDefinitionName = processDefinitionName;
    }

    public String getProcessDefinitionVersion() {
        return processDefinitionVersion;
    }

    public void setProcessDefinitionVersion(String processDefinitionVersion) {
        this.processDefinitionVersion = processDefinitionVersion;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public String getStartUserId() {
        return startUserId;
    }

    public void setStartUserId(String startUserId) {
        this.startUserId = startUserId;
    }

    @Override
    public String toString() {
        return "HistoricProcessInstance_{" +
                "id='" + id + '\'' +
                ", deploymentId='" + deploymentId + '\'' +
                ", description='" + description + '\'' +
                ", name='" + name + '\'' +
                ", tenantId='" + tenantId + '\'' +
                ", businessKey='" + businessKey + '\'' +
                ", callbackId='" + callbackId + '\'' +
                ", callbackType='" + callbackType + '\'' +
                ", processDefinitionId='" + processDefinitionId + '\'' +
                ", processDefinitionKey='" + processDefinitionKey + '\'' +
                ", processDefinitionName='" + processDefinitionName + '\'' +
                ", processDefinitionVersion='" + processDefinitionVersion + '\'' +
                ", startTime=" + startTime +
                '}';
    }
}
